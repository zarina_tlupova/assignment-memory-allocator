#include "util.h"
#include "mem.h"
#include "mem_internals.h"

static inline struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static void test1() {
    fputs("Первый тест: обычное успешное выделение памяти.\n", stdout);
    void *malloc = _malloc(100);
    struct block_header *block = block_get_header(malloc);
    debug_heap(stdout, block);
    if (!block->is_free) {
        printf("\nУспешно\n");
        _free(malloc);
        debug_heap(stdout, block);
    } else err("=(\n");
}

static void test2(){
    fputs("Второй тест: освобождение одного блока из нескольких выделенных.\n", stdout);
    void *malloc1 = _malloc(100);
    void *malloc2 = _malloc(100);
    struct block_header *block_first = block_get_header(malloc1);
    struct block_header *block_second = block_get_header(malloc2);
    debug_heap(stdout, block_first);
    _free(malloc1);
    debug_heap(stdout, block_first);
    debug_heap(stdout, block_second);
    if (block_first->is_free && !block_second->is_free ) {
        printf("\nУспешно\n");
        _free(malloc2);
        debug_heap(stdout, block_second);
    } else err("=(\n");
}

static void test3(){
    fputs("Третий тест: освобождение двух блоков из нескольких выделенных.\n", stdout);
    void *malloc1 = _malloc(100);
    void *malloc2 = _malloc(100);
    void *malloc3 = _malloc(100);
    struct block_header *block_first = block_get_header(malloc1);
    struct block_header *block_second = block_get_header(malloc2);
    struct block_header *block_third = block_get_header(malloc3);
    _free(malloc2);
    _free(malloc1);
    debug_heap(stdout, block_first);
    debug_heap(stdout, block_second);
    debug_heap(stdout, block_third);
    if (block_first->is_free && block_second->is_free && !block_third->is_free) {
        printf("\nУспешно\n");
        _free(malloc3);
        debug_heap(stdout, block_third);
    } else err("=(\n");
}

static void test4(){
    fputs("Четвертый тест: память закончилась, новый регион памяти расширяет старый.\n", stdout);
    void *malloc1 = _malloc(9000);
    void *malloc2 = _malloc(3000);
    struct block_header *block_first = block_get_header(malloc1);
    struct block_header *block_second = block_get_header(malloc2);
    debug_heap(stdout, block_first);
    debug_heap(stdout, block_second);
    if ( !block_first->is_free &&  !block_second->is_free && block_second == block_first->contents + block_first->capacity.bytes) {
        printf("\nУспешно\n");
        _free(malloc2);
        _free(malloc1);    
        debug_heap(stdout, block_first);
        debug_heap(stdout, block_second);
    }else err("=(\n");
}

static void test5() {
    fputs("Пятый тест: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n", stdout);
    void *malloc1 = _malloc(15000);
    struct block_header *block_first = block_get_header(malloc1);
    debug_heap(stdout, block_first);
    struct block_header* last_block = block_first;
    while(last_block->next){
        last_block = last_block->next;
    }
    mmap(last_block->contents + last_block->capacity.bytes, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    void *malloc2 = _malloc(5000);
    debug_heap(stdout, block_first);
    struct block_header *block_second = block_get_header(malloc2);
    debug_heap(stdout, block_second);
    if (!block_first->is_free && !block_second->is_free && last_block->contents + last_block->capacity.bytes != block_second) {
        printf("\nУспешно\n");
        _free(malloc2);
        _free(malloc1);        
        debug_heap(stdout, block_first);
        debug_heap(stdout, block_second);
    }else err("=(\n");
}

int main(){
    heap_init(800);
    test1();
    printf("\n**********************************************\n\n");
    test2();
    printf("\n**********************************************\n\n");
    test3();
    printf("\n**********************************************\n\n");
    test4();
    printf("\n**********************************************\n\n");
    test5();
    return 0;
}

